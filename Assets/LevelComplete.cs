using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    private void OnDestroy()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
