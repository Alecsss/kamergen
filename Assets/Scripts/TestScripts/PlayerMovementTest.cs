using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementTest : MonoBehaviour
{
    public Transform checkSphere;
    public Camera rotateCam;
    public float speed;

    void LateUpdate()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");
        transform.Rotate(Vector3.up, x , Space.World);
        rotateCam.transform.Rotate(Vector3.left, y , Space.Self);

        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");
        Debug.Log(x + ":" + y);
        rb.velocity = transform.forward * y * Time.deltaTime * speed + transform.right * x * Time.deltaTime * speed;

        if (!Physics.CheckSphere(checkSphere.position, 0.3f)) Jump();
    }

    void Jump()
    {

    }
}
