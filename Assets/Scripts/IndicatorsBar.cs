using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndicatorsBar : MonoBehaviour
{
    public Transform player;
    public Image HealthBar;
    private static float health = 100f;

    void Start()
    {
        HealthBar.fillAmount = health / 100f;
    }

    // Update is called once per frame
    void Update()
    {

        if (player.position.y < -10)
        {
            health -= 1 * Time.deltaTime;
        }

    }

    public static void getDamage(int damage)
    {
        health -= damage;
    }

}
