using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    [SerializeField] GameObject[] followObjects;
    void Update()
    {
        foreach (var item in followObjects)
        {
            item.transform.position = transform.position;
            item.transform.rotation = transform.rotation;
        }
    }
}
