using UnityEngine;
using static EnumPlayerItem;

public class PlayerMovement : MonoBehaviour
{
    public HumanTimeLine humanTimeLine;
    public CharacterController player;
    public GameObject point;
    public Camera cam;

    public static Vector3 currPlayerPos = Vector3.zero;

    private WeaponClassObj currentWeapon = new WeaponClassObj() { setWeapon = false};

    private float speed = 5f;
    private float rotationSpeed = 5f;
    private float gravity = -9.8f;
    private float x;
    private float z;
    private float getX;
    private float getZ;

    public Vector3 velocity;

    private int layerMask = 1 << 7; // raycast ���������� �������� ���������
    private int layerMaskWithoutPlayer;

    private int health = 100;

    private void Start()
    {
        cam = Camera.main;
        layerMaskWithoutPlayer = ~layerMask;
    }

    void Update()
    {
        currPlayerPos = gameObject.transform.position;
        humanTimeLine.group.transform.position = gameObject.transform.position;
        // �������� �������

        getX = Input.GetAxis("Horizontal");
        getZ = Input.GetAxis("Vertical");

        x = getX * speed * Time.deltaTime - getZ * speed * Time.deltaTime;
        z = getZ * speed * Time.deltaTime + getX * speed * Time.deltaTime;

        Vector3 move = new Vector3(x, 0, z);
        if (move == Vector3.zero) Stay();
        else Move(move);
        player.Move(move);


        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMaskWithoutPlayer))
            point.transform.position = hit.point;

        Vector3 rotatePointVector = new Vector3(point.transform.position.x, transform.position.y, point.transform.position.z) - transform.position;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rotatePointVector), rotationSpeed * Time.deltaTime * 10);

        LookAt(transform.rotation);

        // ������ �������

        velocity.y = gravity * Time.deltaTime;
        player.Move(velocity);

        OpenDoor.InteractDoor(hit);
    }

    public void getDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    public GameObject getPoint()
    {
        return point;
    }

    //������ ��� ���������� � ���������
    private void Move(Vector3 dir)
    {
            humanTimeLine.leftHandAnimator.Play();
        if (currentWeapon.setWeapon)
            humanTimeLine.rightHandAnimator.Play();
        else humanTimeLine.rightHand.transform.localRotation = Quaternion.Euler(-90, 0, 0);
        humanTimeLine.legsAnimator.Play();

        humanTimeLine.legs.transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
    }

    private void Stay()
    {
        humanTimeLine.leftHandAnimator.Stop();
        humanTimeLine.rightHandAnimator.Stop();
        humanTimeLine.legsAnimator.Stop();
    }

    private void LookAt(Quaternion lookRotation)
    {
        humanTimeLine.body.transform.localRotation = Quaternion.Lerp(humanTimeLine.body.transform.localRotation, lookRotation, rotationSpeed / 2 * Time.deltaTime);
        humanTimeLine.head.transform.localRotation = lookRotation;
    }

    //���������� �������� � �������
    public void SetWeapon(WeaponClass weaponClass, bool state)
    {
        currentWeapon.setWeapon = state;
        currentWeapon.weaponId = weaponClass;
    }

    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Detect");
        if (collision.tag == "Box") {
            DropItem dropItem = collision.GetComponent<DropItem>();
            switch (dropItem.GetArmorId())
            {
                case ArmorElement.Head:
                    humanTimeLine.headObj.GetComponent<MeshRenderer>().material = dropItem.GetMaterial();
                    humanTimeLine.headObj.GetComponent<MeshFilter>().mesh = dropItem.GetMesh();
                    break;
                case ArmorElement.Body:
                    humanTimeLine.bodyObj.GetComponent<MeshRenderer>().material = dropItem.GetMaterial();
                    humanTimeLine.bodyObj.GetComponent<MeshFilter>().mesh = dropItem.GetMesh();
                    break;
                case ArmorElement.leftHand:
                    humanTimeLine.leftHandObj.GetComponent<MeshRenderer>().material = dropItem.GetMaterial();
                    humanTimeLine.leftHandObj.GetComponent<MeshFilter>().mesh = dropItem.GetMesh();
                    humanTimeLine.leftHandAnimator.SetAnimation(dropItem.GetMesh(), dropItem.GetMaterial());
                    humanTimeLine.leftHandAnimator.SetAnimation(dropItem.GetAnim());
                    break;
                case ArmorElement.RightHand:
                    humanTimeLine.rightHandObj.GetComponent<MeshRenderer>().material = dropItem.GetMaterial();
                    humanTimeLine.rightHandObj.GetComponent<MeshFilter>().mesh = dropItem.GetMesh();
                    humanTimeLine.rightHandAnimator.SetAnimation(dropItem.GetMesh(), dropItem.GetMaterial());
                    humanTimeLine.rightHandAnimator.SetAnimation(dropItem.GetAnim());
                    break;
                case ArmorElement.Legs:
                    humanTimeLine.legsObj.GetComponent<MeshRenderer>().material = dropItem.GetMaterial();
                    humanTimeLine.legsObj.GetComponent<MeshFilter>().mesh = dropItem.GetMesh();
                    humanTimeLine.legsAnimator.SetAnimation(dropItem.GetMesh(), dropItem.GetMaterial());
                    humanTimeLine.legsAnimator.SetAnimation(dropItem.GetAnim());
                    break;
            }
            Destroy(collision.gameObject);
        }
    }

}
struct WeaponClassObj
{
    public bool setWeapon;
    public WeaponClass weaponId;
}