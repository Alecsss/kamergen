using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public Transform player;
    private Animator animator;
    private bool isOpen = true;
    private bool isOpenMirror = true;

    void Start()
    {
        animator = GetComponent<Animator>();
        if (!animator)
            Debug.LogError("�� ������� �������� ��������� � �������!");
        
    }

    static public void InteractDoor(RaycastHit hit)
    {
        if(hit.transform != null && hit.transform.GetComponent<OpenDoor>())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                hit.transform.GetComponent<OpenDoor>().SwitchState();
            }
        }
    }

    public void SwitchState()
    {
        if((player.position.z - transform.position.z < 0 && isOpenMirror == true ) || isOpen == false)
        {
            animator.SetBool("isOpen", isOpen);
            isOpen = !isOpen;
        }
        else if(player.position.z - transform.position.z > 0 || isOpenMirror == false)
        {
            animator.SetBool("isOpenMirror", isOpenMirror);
            isOpenMirror = !isOpenMirror;
        }
        
    }

}
