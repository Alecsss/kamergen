using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampDetector : MonoBehaviour
{
    public float range = 10;
    public Transform player;
    private float currentRange;

    void Update()
    {
        Material mat = this.gameObject.GetComponent<MeshRenderer>().material;

        currentRange = Mathf.Sqrt(Mathf.Abs( Mathf.Pow( player.position.x - transform.position.x, 2) + Mathf.Pow(player.position.y - transform.position.y, 2) + Mathf.Pow(player.position.z - transform.position.z, 2)));

        if(currentRange <= range) // �� ��� ������ ���-�� � ������� ����
        {
            mat.SetColor("_Color", Color.yellow);
            //Debug.Log("��������");
        }
        else
        {
            mat.SetColor("_Color", Color.black);
            //Debug.Log("�� ��������");
        }
    }
}
