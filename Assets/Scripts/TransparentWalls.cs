using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparentWalls : MonoBehaviour
{
    public Transform player;
    public Camera cam;
    private Material mat;

    // Update is called once per frame
    void Update()
    {

        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);

        
        if (hit.transform != null)
        {
            mat = hit.collider.gameObject.GetComponent<MeshRenderer>().material;
            mat.SetFloat("_Speed", Time.time);
        }


        //Debug.DrawRay(ray.origin, ray.direction, Color.red);

    }
}
