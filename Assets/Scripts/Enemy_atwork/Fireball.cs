using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public GameObject enemy;
    public GameObject fireTrail;
    public PlayerMovement playerMovement;

    private float speed = 8f;
    private bool isTrailsNeeded = true;

    // Update is called once per frame
    void Update()
    {
        if (enemy)
        transform.rotation = enemy.transform.rotation;

        if (isActiveAndEnabled)
        {
            if (isTrailsNeeded)
            {
                for(int i=1; i<30; i++)
                    StartCoroutine(CreateWait(i*0.2f));

                isTrailsNeeded = false;
            }

            transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

        }

    }
    IEnumerator CreateWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (fireTrail != null)
        {
            GameObject newFireTrail = Instantiate(fireTrail);
            newFireTrail.SetActive(true);
            newFireTrail.transform.rotation = fireTrail.transform.rotation;
            newFireTrail.transform.position = getRay().point;
            Destroy(newFireTrail, 3f);
        }

    }

    // ��������� ��������� �� ������� ������� ����� ��� �����
    private RaycastHit getRay()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, Mathf.Infinity);

        return hit;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
            playerMovement.getDamage(40); // � ������� ���������

        if (collision.collider.tag != "Enemy")
            Destroy(gameObject);
    }

}
 