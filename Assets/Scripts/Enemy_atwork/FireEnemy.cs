using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEnemy : MonoBehaviour
{
    public GameObject player;
    public CharacterController enemy;
    public GameObject enemyDestroyable;

    public GameObject attackAnim;
    public GameObject fireball;

    private Vector3 lastPlayerPos;
    private Vector3 startEnemyPos;

    // ����������
    private float speed = 4f; // ��� ��������
    private float sleepSpeed = 2f; // ��� ��������
    private float rotationSpeed = 4f;

    // ������ (�������)
    private Vector3 velocity;
    private float gravity = -9.8f;

    private bool doMove = false; // ����� �� ��������� ������
    private bool newRange = true; // ����� �� ������� ����� ���������� ��� �����������
    private bool agressiveMode = false;
    private bool notSleep = true;
    private bool attackDelay = true;

    private Vector3 startPosition; // ��������� ������� ����� ��������
    private float range; // ����������, ����������� ��� �����������
    private int randomView; // ������� (360*) ������������ ����������� ���������

    private int health = 100;


    void Start()
    {
        range = -1f;

        lastPlayerPos = Vector3.zero;
        startEnemyPos = transform.position;
    }

    void Update()
    {
        if (sleepMode() && notSleep)
        {

            //�������� � ���������
            if (newRange)
            {
                newRange = false;
                startPosition = transform.position;
                StartCoroutine(Wait(Random.Range(5, 15)));
            }

            if (Vector3.Distance(transform.position, startPosition) < range && doMove)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis(randomView, Vector3.up), rotationSpeed * Time.deltaTime);
                enemy.Move(transform.forward * sleepSpeed * Time.deltaTime);
            }
            else if (range != -1f)
            {
                newRange = true;
                doMove = false;
                range = -1f;
            }

            //print(getRayDistanceToGameObject());
            changeRotation();

            if (!agressiveMode) checkPlayer();
            else beAgressive();

        }

        //������
        velocity.y = gravity * Time.deltaTime;
        enemy.Move(velocity);

        //����


    }

    private void createNew_MoveTask()
    {
        print("enemyMovement doing..."); //*������� � �������*
        //���������� ����������
        range = Random.Range(10f, 20f);
        //���������� ���� ��������
        randomView = Random.Range(0, 360);

        doMove = true;
    }

    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        createNew_MoveTask();
    }

    private bool sleepMode()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < 100) return true;
        return false;
    }

    // ��������� ��������� �� ������� ������� ����� ����� �����
    private float getRayDistanceToGameObject()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);

        float distance = 999;
        if (hit.transform != null)
            distance = hit.distance;

        return distance;
    }

    private void changeRotation()
    {
        if (getRayDistanceToGameObject() < 2f)
        {
            int option = right_left_rotation();

            randomView += option * 1;
        }
    }

    private int right_left_rotation()
    {
        float leftDist;
        float rightDist;

        transform.rotation *= Quaternion.Euler(0f, 10f, 0f);
        rightDist = getRayDistanceToGameObject();
        transform.rotation *= Quaternion.Euler(0f, -20f, 0f);
        leftDist = getRayDistanceToGameObject();
        transform.rotation *= Quaternion.Euler(0f, 10f, 0f);

        if (rightDist >= leftDist) return 1;
        else return -1;
    }

    private RaycastHit getRayToPlayer()
    {
        Ray ray = new Ray(transform.position, (PlayerMovement.currPlayerPos - transform.position).normalized);
        RaycastHit _hit;
        Physics.Raycast(ray, out _hit);

        return _hit;
    }

    private void checkPlayer()
    {
        RaycastHit hit = getRayToPlayer();

        if (hit.transform != null && hit.transform == player.transform && hit.distance < 30)
        {
            agressiveMode = true;
            doMove = false;
            newRange = false;
        }

    }

    private void beAgressive()
    {
        range = -1f;

        RaycastHit hit = getRayToPlayer();

        if (hit.transform != null && hit.transform == player.transform && hit.distance < 30 && hit.distance > 15)
        {

            Quaternion toRotate = Quaternion.LookRotation((PlayerMovement.currPlayerPos - transform.position).normalized);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotate, rotationSpeed * Time.deltaTime);
            enemy.Move(transform.forward * speed * Time.deltaTime);

            lastPlayerPos = PlayerMovement.currPlayerPos;
            startEnemyPos = transform.position;

        }
        else if (hit.transform != null && hit.transform == player.transform && hit.distance <= 15)
        {
            Quaternion toRotate = Quaternion.LookRotation((PlayerMovement.currPlayerPos - transform.position).normalized);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotate, rotationSpeed * Time.deltaTime * 2);
            if (attackDelay)
                StartCoroutine(AttackWait(3f));
        }
        else if (hit.transform != player.transform && hit.distance < 30 && Vector3.Distance(startEnemyPos, transform.position) - 1 < Vector3.Distance(startEnemyPos, lastPlayerPos))
        {
            enemy.Move(transform.forward * speed * Time.deltaTime);
        }
        else
        {
            StartCoroutine(CheckingWait(1f));
            agressiveMode = false;
            notSleep = false;
        }
    }
    private void attack()
    {

        GameObject newFireball = Instantiate(fireball);
        newFireball.transform.position = fireball.transform.position;
        newFireball.SetActive(true);

        Destroy(newFireball, 5);

        attackDelay = true;
        attackAnim.SetActive(false);

    }
    public void getDamage(int damage)
    {
        health -= damage;
        print(damage);

        if (health <= 0)
        {
            if (!gameObject.GetComponent<Dropable>())
                Debug.LogError("����� �������� dropable");
            gameObject.GetComponent<Dropable>().dropping();

            Destroy(enemyDestroyable);
        }
    }


    IEnumerator CheckingWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        notSleep = true;
        newRange = true;
    }

    IEnumerator AttackWait(float seconds)
    {
        attackAnim.SetActive(true);
        attackDelay = false;
        yield return new WaitForSeconds(seconds);
        RaycastHit _hit = getRayToPlayer();
        if (_hit.distance <= 15 && _hit.transform == player.transform) attack();
        else
        {
            attackDelay = true;
            attackAnim.SetActive(false);
        }
    }

}
