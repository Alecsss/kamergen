using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    public Transform player;
    private float size;

    private void Start()
    {
        size = player.localScale.x + player.localScale.y + player.localScale.z;
        transform.position = new Vector3(size*2.5f, size*2f, -size*2.5f);
        transform.rotation = Quaternion.Euler(35, -45, 0);
    }

    void Update()
    {
        transform.position = new Vector3(player.position.x + size*2.5f, player.position.y + size*2f, player.position.z - size*2.5f);
        
    }
}
