using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour, Weaponable
{
    public Camera cam;
    public GameObject bullet;
    private GameObject newBullet;
    public PlayerMovement player;
    private bool isActive = false;
    private int maxAmmo = 20;
    private int ammo = 20;
    private int clip = 1; // ���-�� ����� � ������
    private float reloadTime = 3f;

    public bool useGun()
    {
        isActive = !isActive;
        return isActive;
    }

    public bool getActive()
    {
        return isActive;
    }

    public void shoot()
    {
        if (isActive)
            if (ammo > 0)
            {
                ammo--;

                newBullet = Instantiate(bullet);
                newBullet.transform.rotation = transform.rotation;
                newBullet.transform.position = bullet.transform.position;

                newBullet.GetComponent<Bullet>().go();

            }
            else
            {
                doReload();
            }
    }

    private void reload()
    {
        if (isActive)
            if (clip > 0)
            {
                Debug.Log("reloaded");
                clip--;
                ammo = maxAmmo;
            }
    }

    public void doReload()
    {
        ammo = 0;
        StartCoroutine(Wait(reloadTime));
    }

    public void receiveDrop(int clips)
    {
        this.clip += clips;
    }

    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        reload();
    }

}
