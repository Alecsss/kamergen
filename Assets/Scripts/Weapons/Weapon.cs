using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Weaponable gun;
    public GameObject gunObj;
    void Start()
    {
        gun = gunObj.GetComponent<Gun>();
        gunObj.SetActive(false);
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            gunObj.SetActive(gun.useGun());
        }

        //����������� ��� ������
        if (Input.GetKeyDown(KeyCode.R) && (gun is Gun))
        {
            if ((gun as Gun).getActive())
                (gun as Gun).doReload();

        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && (gun is Gun))
        {
            if ((gun as Gun).getActive())
                (gun as Gun).shoot();
        }

    }
}

