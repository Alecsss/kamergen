using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float speed = 50f;
    private bool isShooting = false;

    public void Start()
    {
        transform.Rotate(0, 90, 0);
    }

    public void Update()
    {
        if (!isShooting) return;

        transform.Translate(Vector3.back * Time.deltaTime * speed, Space.Self);
    }
    public void go()
    {
        isShooting = true;
        gameObject.SetActive(true);
        if (gameObject != null) Destroy(gameObject, 10);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.collider.GetComponent<FireEnemy>()?.getDamage(15);

            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
        else
            if (!((collision.gameObject.tag == "Player")||(collision.gameObject.tag == "Weapon")) )
                //Destroy(gameObject);
                gameObject.SetActive(false);
    }

}
