using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour, Weaponable
{
    private bool isActive = false;

    public bool useGun()
    {
        isActive = !isActive;
        return isActive;
    }
    public void shoot()
    {
        if (isActive)
        {
            //
        }
    }

}
