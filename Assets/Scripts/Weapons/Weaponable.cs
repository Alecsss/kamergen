using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Weaponable
{
    bool useGun();

}
