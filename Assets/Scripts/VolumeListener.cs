using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeListener : MonoBehaviour
{
    [SerializeField] AudioSource listener;
    [SerializeField] AudioType audioType;
    void Update()
    {
        if (audioType == AudioType.VFX)
                listener.volume = GameController.GetVFX();
        else    listener.volume = GameController.GetMusic();
    }
}

enum AudioType
{
    VFX,
    Music
}
