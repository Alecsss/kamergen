using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniCell : MonoBehaviour
{
    public Image image;
    public Animator animator;
    public bool state { get; private set; }
    void Awake()
    {
        if (!image) Debug.LogError("�� ������� ����� ��������� Image");
        if (!animator) Debug.LogError("�� ������� ����� ��������� Animator");
    }

    public void Deactive()
    {
        state = false;
        image.color = new Color(0, 0, 0, 0);
    }

    public void Active(bool newItem = false)
    {
        state = true;
        image.color = new Color(1, 1, 1, 1);
        if (newItem) PlayAnimation();
    }

    private void PlayAnimation()
    {
        animator.SetBool("NewItem", true);
    }

    public void StopAnimation()
    {
        animator.SetBool("NewItem", false);
    }
}
