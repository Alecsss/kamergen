using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanTimeLine : MonoBehaviour
{
    public GameObject group;

    [Header("Head")]
    public GameObject headObj;
    public GameObject head;
    public AnimatorObj headAnimator;

    [Header("Body")]
    public GameObject bodyObj;
    public GameObject body;
    public AnimatorObj bodyAnimator;

    [Header("LeftHand")]
    public GameObject leftHandObj;
    public GameObject leftHand;
    public AnimatorObj leftHandAnimator;

    [Header("RightHand")]
    public GameObject rightHandObj;
    public GameObject rightHand;
    public AnimatorObj rightHandAnimator;

    [Header("Legs")]
    public GameObject legsObj;
    public GameObject legs;
    public AnimatorObj legsAnimator;
}
