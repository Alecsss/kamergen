using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorObj : MonoBehaviour
{
    // ������ ������ ���������� �� ������� ������� ����� �������������
    [Header("���������")]
    [SerializeField] Transform objCurrent;

    [SerializeField] AnimationObj[] animationFilm = new AnimationObj[0];
    [SerializeField] float animDelay = 1;

    [SerializeField] bool calculatePos;
    [SerializeField] bool calculateRot;
    [SerializeField] bool playStart = false;
    float AnimDelay { get
        {
            if (animDelay < 0) animDelay = 1;
            return animDelay;
        } 
    }

    private MeshRenderer objMeshRender;
    private MeshFilter objMeshFilter;
    private Coroutine currentCoroutine;

    Mesh startMesh;
    Material startMat;

    int curShot = 0;

    private void Awake()
    {
        objMeshRender = GetComponent<MeshRenderer>();
        objMeshFilter = GetComponent<MeshFilter>();
        if (!objMeshRender) Debug.LogError("�� ������� �������� ���������" + typeof(MeshRenderer) + "� �������" + gameObject.name);
        if (!objMeshFilter) Debug.LogError("�� ������� �������� ���������" + typeof(MeshFilter) + "� �������" + gameObject.name);
        startMesh = objMeshFilter.mesh;
        startMat = objMeshRender.material;
        if (playStart) Play();
    }

    public void SetAnimation(Mesh mesh, Material mat)
    {
        startMesh = mesh;
        startMat = mat;
    }
    
    public void SetAnimation(AnimationObj[] anims)
    {
        animationFilm = anims;
    }

    public void Play()
    {
        if (currentCoroutine == null)
        currentCoroutine = StartCoroutine(Animate(curShot));
    }

    public void Pause()
    {
        StopCoroutine(currentCoroutine);
    }

    public void Stop()
    {
        if (currentCoroutine == null) return;
        curShot = 0;
        StopCoroutine(currentCoroutine);
        currentCoroutine = null;
        SetDefault();
    }

    private void SetDefault()
    {
        objMeshFilter.mesh = startMesh;
        objMeshRender.material = startMat;
        SetRotation(Quaternion.Euler(Vector3.zero));
    }

    IEnumerator Animate(int setShot = 0)
    {
        curShot = setShot;
        while (true)
        {
            if (calculatePos)
            transform.localPosition =  animationFilm[curShot].FixPivot();
            if (calculateRot)
            SetRotation(animationFilm[curShot].rotate);

            animationFilm[curShot].ReCalculate();

            objMeshRender.material = animationFilm[curShot].objMaterial;
            objMeshFilter.mesh = animationFilm[curShot].objMesh;
            curShot = (curShot + 1) % animationFilm.Length;
            yield return new WaitForSeconds(AnimDelay);
        }
    }

    private void SetRotation(Quaternion rot)
    {
        if (objCurrent)
        objCurrent.localRotation = rot;
    }
}
