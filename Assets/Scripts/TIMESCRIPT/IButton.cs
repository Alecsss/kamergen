using UnityEngine.EventSystems;

public interface IButton<T> : IPointerClickHandler
{
    public void DoEvent(T item);
}
