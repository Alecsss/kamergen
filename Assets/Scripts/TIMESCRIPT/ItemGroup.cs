using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGroup : MonoBehaviour
{
    [Header("CellControllers")]
    [SerializeField] Slot mainSlot;
    [SerializeField] MiniCells visualCells;
    [SerializeField] MainCells mainCells;

    private int itemCount { get; set; }

    private void Start()
    {
        if (!mainSlot)   Debug.LogError("��������� Slot ����");
        if (!visualCells) Debug.LogError("��������� MiniCells ����");
        if (!mainCells)   Debug.LogError("��������� MainCells ����");

        visualCells.Active(3);
    }

    public void Add() //��������� ���-�� ������ Item
    {
        visualCells.AddNew();
    }

    public void CheckedNewItems()
    {
        visualCells.CheckedItem();
    }
}
