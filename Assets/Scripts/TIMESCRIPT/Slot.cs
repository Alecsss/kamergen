using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerClickHandler
{
    private System.Action action;

    public void SubscribeOnEvent(System.Action eventMethod)
    {
        action += eventMethod;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        action?.Invoke();
    }
}
