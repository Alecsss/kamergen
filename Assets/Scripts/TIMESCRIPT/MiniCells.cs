using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniCells : MonoBehaviour
{
    [SerializeField] MiniCell[] miniCells;

    private static Color hideColor = new Color(0, 0, 0, 0);

    public void Active(int count)
    {
        foreach (var item in miniCells)
        {
            if (count-- > 0) item.Active();
            else item.Deactive();
        }
    }
    public void AddNew()
    {
        foreach (var item in miniCells) if (!item.state) { item.Active(true); break; }
    }

    public void CheckedItem()
    {
        foreach (var item in miniCells) if (item.state) item.StopAnimation();
    }
}
