using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumPlayerItem : MonoBehaviour
{
    public enum WeaponClass
    {
        sword = 0,
        gun = 1,
        magickWand = 2,
    }

    public enum ArmorType
    {
        head = 0,
        body = 1,
        leftHand = 2,
        rightHand = 3,
        leftLeg = 4,
        rightLeg = 5,
    }

    public enum SkinType
    {
        steel = 0,
        skelet = 1,
        mucus = 2,
        hidden = 3,
        fire = 4,
        muck = 5,
        cursed = 6
    }
}
