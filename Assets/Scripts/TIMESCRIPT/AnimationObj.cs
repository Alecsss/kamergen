
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "AnimCell", menuName = "ScriptObject/Game/Animation", order = 1)]
public class AnimationObj : ScriptableObject
{

    public Material objMaterial;
    public Mesh objMesh;

    [Header("������ �������")]
    public Quaternion rotate;

    [Header("������ ���������")]
    public Vector3 mainPos;

    [Header("��������� �����")]
    public int pivot_Forward;
    public int pivot_Top;
    public int pivot_Right;

    public Vector3 FixPivot()
    {
        pivot_Forward = FixIndex(pivot_Forward);
        pivot_Top =     FixIndex(pivot_Top);
        pivot_Right =   FixIndex(pivot_Right);

        Vector3 pos = -objMesh.bounds.center;
        pos -= new Vector3(pos.x * pivot_Right, pos.y * pivot_Top, pos.z * pivot_Forward);
        pos += mainPos;

        return pos;
    }

    public void ReCalculate()
    {
        Vector3 pos = objMesh.bounds.center;
    }

    private int FixIndex(int ind)
    {
        if (ind == 0) return 0;
        if (ind > 0) return 1;
        else return -1;
    }
}
