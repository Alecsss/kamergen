using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageController : MonoBehaviour
{
    private static StorageController instance;

    [SerializeField] ItemType []storageItems;

    public void Awake()
    {
        if (!instance) instance = this;
        else Destroy(gameObject);
    }
    public static ItemType GetItem(SkinType skin, ArmorElement armor)
    {
        foreach (var item in instance.storageItems)
            if (item.element == armor && item.skin == skin) return item;
        Debug.LogError("�� ������� ����� ����");
        return null;
    }
    
}

public enum SkinType
{
    Dirt,
    Skeleton,
    Slime,
    Fire
}

public enum ArmorElement
{
    Head,
    leftHand,
    RightHand,
    Legs,
    Body
}
