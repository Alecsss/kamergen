using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropable : MonoBehaviour
{
    public GameObject drop;
    private GameObject dropped;

    public void dropping()
    {
        dropped = Instantiate(drop);
        dropped.transform.position = transform.position;
        dropped.SetActive(true);
    }
}
