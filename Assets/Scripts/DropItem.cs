using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    [SerializeField] MeshRenderer meshRender;
    [SerializeField] MeshFilter meshFilter;

    [SerializeField] Mesh fireBox;
    [SerializeField] Material fireMat;

    [SerializeField] Mesh skeletonBox;
    [SerializeField] Material skeletonMat;

    [SerializeField] Mesh slimeBox;
    [SerializeField] Material slimeMat;

    private SkinType skin;
    private ArmorElement armor;
    private ItemType item;

    void Start()
    {
        System.Array array = System.Enum.GetValues(typeof(SkinType));
        System.Array arrayArmor = System.Enum.GetValues(typeof(ArmorElement));
        switch (array.GetValue(Random.Range(1, array.Length)))
        {
            case SkinType.Fire:
                meshRender.material = fireMat;
                meshFilter.mesh = fireBox;
                skin = SkinType.Fire;
                break;
            case SkinType.Skeleton:
                meshRender.material = skeletonMat;
                meshFilter.mesh = skeletonBox;
                skin = SkinType.Skeleton;
                break;
            case SkinType.Slime:
                meshRender.material = slimeMat;
                meshFilter.mesh = slimeBox;
                skin = SkinType.Slime;
                break;
        }
        armor = (ArmorElement)arrayArmor.GetValue(Random.Range(0, arrayArmor.Length));
        item = StorageController.GetItem(skin, armor);
    }
    public ArmorElement GetArmorId()
    {
        return armor;
    }

    public Mesh GetMesh()
    {
        return item.mesh;
    }

    public Material GetMaterial()
    {
        return item.mat;
    }

    public AnimationObj[] GetAnim()
    {
        return item.anim;
    }
}
