using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemCell", menuName = "ScriptObject/Game/Item", order = 1)]
public class ItemType : ScriptableObject
{
    public ArmorElement element;
    public SkinType skin;

    public Mesh mesh;
    public Material mat;
    public AnimationObj[] anim;
}
