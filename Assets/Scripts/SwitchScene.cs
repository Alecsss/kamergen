using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SwitchScene : MonoBehaviour
{
    public void SceneTo(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
