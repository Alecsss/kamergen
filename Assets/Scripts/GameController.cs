using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private float vfx = 1;
    private float music = 1;

    [SerializeField] Slider vfxSlider;
    [SerializeField] Slider musicSlider;

    private static GameController instance;

    private void Awake()
    {
        if (!instance) instance = this;
        else Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void SetVFX()
    {
        vfx = vfxSlider.value;
    }

    public void SetMusic()
    {
        music = musicSlider.value;
    }

    public static float GetVFX()
    {
        return instance.vfx;
    }

    public static float GetMusic()
    {
        return instance.music;
    }
}
