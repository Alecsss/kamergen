using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fairy : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(Fly());
    }

    IEnumerator Fly()
    {
        while (true)
        {
            transform.position += new Vector3(Random.Range(-0.1f,0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
            yield return new WaitForSeconds(0.1f
                );
        }
    }
}
