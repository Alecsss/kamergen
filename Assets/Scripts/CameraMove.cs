using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private int count = 0;

    public GameObject point;
    public Camera cam;
    public GameObject block;
    private GameObject newBlock;
    public GameObject folder;

    private float getX;
    private float getZ;
    private float x;
    private float z;
    private float speed = 20f;

    public float sensitivity = 5f;
    private float rotationX;
    private float rotationY;
    private float xRot = 0f;

    private void Start()
    {
        cam = Camera.main;

        /*
        for(int i = 0; i < 200; i++)
            for(int j = 0; j < 200; j++)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(0 + 1.024f * i, 0, 0 + 1.024f * j);
            }
        */
        

    }
    void Update()
    {
        //raycast
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
            point.transform.position = hit.point;

        //movement

        getX = Input.GetAxis("Horizontal");
        getZ = Input.GetAxis("Vertical");

        x = getX * speed * Time.deltaTime;
        z = getZ * speed * Time.deltaTime;

        cam.transform.position += z * transform.up + x * transform.right;

        //rotation

        rotationX = Input.GetAxis("Mouse X") * sensitivity;
        rotationY = Input.GetAxis("Mouse Y") * sensitivity;

        xRot -= rotationY;

        cam.transform.Rotate(Vector3.up * rotationX);
        cam.transform.Rotate(Vector3.right * -rotationY);

        cam.transform.eulerAngles = new Vector3(cam.transform.eulerAngles.x, cam.transform.eulerAngles.y, 0);



        //������ � �������� �����

        if (Input.GetKeyDown(KeyCode.E)){
            

            // ���������� �����
            print( hit.transform.position );
            count++;
            print(count);
            

            //----------------
            if( point.transform.position.z - hit.transform.position.z >= 0.511f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y, hit.transform.position.z + 1.024f);
            }
                
            if (point.transform.position.z - hit.transform.position.z <= -0.511f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y, hit.transform.position.z - 1.024f);
            }
            //-----------------
            if (point.transform.position.x - hit.transform.position.x >= 0.511f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x + 1.024f, hit.transform.position.y, hit.transform.position.z);
            }

            if (point.transform.position.x - hit.transform.position.x <= -0.511f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x - 1.024f, hit.transform.position.y, hit.transform.position.z);
            }
            //-----------------
            if (point.transform.position.y - hit.transform.position.y >= 1.1f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y + 1.024f, hit.transform.position.z);
            }

            if (point.transform.position.y - hit.transform.position.y <= 0.15f)
            {
                newBlock = Instantiate(block, folder.transform);
                newBlock.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y - 1.024f, hit.transform.position.z);
            }


            //newBlock.transform.position = new Vector3(2, 2, 2);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if(hit.transform.position != null)
                Destroy(hit.collider.gameObject);
        }

    }
}
