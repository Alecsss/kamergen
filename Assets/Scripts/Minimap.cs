using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{

    public Transform player;
    public RectTransform playerArrow;

    private void Start()
    {
        transform.rotation = Quaternion.Euler(90, -45, 0);
    }

    void Update()
    {
        transform.position = new Vector3(player.position.x, transform.position.y, player.position.z);

        //rotation � "player" �� ��������� � rotation � "�������"
        playerArrow.rotation = Quaternion.Euler(0, 0, -player.eulerAngles.y - 45);


    }

    public GameObject minimap;
    private bool minimapStatus = true;
    public void Switch()
    {
        minimapStatus = !minimapStatus;
        minimap.SetActive(minimapStatus);
    }

}
