using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockType : MonoBehaviour
{
    [SerializeField] BlockTypes typeBlock = BlockTypes.Nothing;
    void Start()
    {
        switch (typeBlock)
        {
            case BlockTypes.Nothing:
                break;
            case BlockTypes.Dirt:
                break;
            case BlockTypes.YellowGreen:
                break;
            case BlockTypes.Green:
                break;
        }
    }
}

enum BlockTypes
{
    Nothing = 0,
    Dirt = 1,
    YellowGreen = 2,
    Green = 3
}
